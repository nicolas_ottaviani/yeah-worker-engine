var should  = require('should');
var engine = require('./../')

describe('Engine', function() {
  describe('Object', function() {
    it('does create object from array', function() {
      var props = engine.createProps ([{key1: 'value', key2: 'value2'}]);
      props.should.be.an.instanceOf(Object)
        .and.have.property('key1', 'value');
      props.should.be.have.property('key2', 'value2')
    });
  });

  describe('Http', function() {
    it('does retrieve get', function(done) {
      engine.httpGet('https://www.leboncoin.fr', function (body){
        body.should.be.an.instanceOf(String);
        done();
      })
    });
  });


  describe('Cheerio', function() {
    it('does parse html', function() {
      var $ = engine.cheerioLoad('<html><head></head><body><h1>Yeah</h1></body></html>');
      var result = $('body h1').text();
      result.should.be.an.instanceOf(String).and.be.exactly('Yeah');
    });
  });
});
