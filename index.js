var needle = require('needle');
var cheerio = require('cheerio');

function createProps (array){
  var obj = {};
  for (var i in array){
    var currentObj = array[i];
    for (var property in currentObj) {
      if (currentObj.hasOwnProperty(property)) {
        obj[property] = currentObj[property];
      }
    }
  }
  return obj;
}

function httpGet (url, callback){
    needle.get(url, function(error, response) {
    if (error){
        throw new Error("ooups");
    }
    if (response.statusCode === 404){
      throw new Error("Not found");
    }
    callback(response.body);
  });
}

function cheerioLoad (body){
  return cheerio.load(body)
}


module.exports = {
  createProps: createProps,
  httpGet: httpGet,
  cheerioLoad: cheerioLoad
};
